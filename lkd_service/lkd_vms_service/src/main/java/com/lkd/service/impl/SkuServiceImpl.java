package com.lkd.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.lkd.dao.SkuDao;
import com.lkd.entity.ChannelEntity;
import com.lkd.entity.SkuClassEntity;
import com.lkd.entity.SkuEntity;
import com.lkd.exception.LogicException;
import com.lkd.service.ChannelService;
import com.lkd.service.SkuClassService;
import com.lkd.service.SkuService;
import com.lkd.viewmodel.Pager;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SkuServiceImpl extends ServiceImpl<SkuDao,SkuEntity> implements SkuService{
    @Autowired
    private SkuClassService skuClassService;
    @Autowired
    private ChannelService channelService;



    @Override
    public boolean update(SkuEntity skuEntity) throws LogicException {
        UpdateWrapper<SkuEntity> uw = new UpdateWrapper<>();
        uw.lambda()
                .set(SkuEntity::getClassId,skuEntity.getClassId())
                .set(SkuEntity::getSkuName,skuEntity.getSkuName())
                .set(SkuEntity::getUnit,skuEntity.getUnit())
                .set(SkuEntity::getSkuImage,skuEntity.getSkuImage())
                .set(SkuEntity::getPrice,skuEntity.getPrice())
                .eq(SkuEntity::getSkuId,skuEntity.getSkuId());

        return this.update(uw);
    }

    @Override
    public boolean delete(Long id) {
        QueryWrapper<ChannelEntity> qw = new QueryWrapper<>();
        qw
                .lambda()
                .eq(ChannelEntity::getSkuId,id);
        if(channelService.count(qw) > 0){
            throw new LogicException("该商品正在使用中");
        }

        return this.removeById(id);
    }

    @Override
    public Pager<SkuEntity> findPage(long pageIndex, long pageSize, Integer classId, String skuName) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SkuEntity> page =
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageIndex,pageSize);

        LambdaQueryWrapper<SkuEntity> qw = new LambdaQueryWrapper<>();
        if(!Strings.isNullOrEmpty(skuName)){
            qw.like(SkuEntity::getSkuName,skuName);
        }
        if(classId !=null && classId >0){
            qw.eq(SkuEntity::getClassId,classId);
        }
        this.page(page,qw);

        return Pager.build(page);
    }


    @Override
    public List<SkuClassEntity> getAllClass() {
        return skuClassService.list();
    }

    @Autowired
    private RestHighLevelClient esClient;

    @Override
    public List<Long> getTop10Sku(Integer businessId) {

        SearchRequest searchRequest=new SearchRequest("order");
        SearchSourceBuilder sourceBuilder=new SearchSourceBuilder();
        //查询条件：最近三个月

        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("update_time");
        rangeQueryBuilder.gte( LocalDateTime.now().plusMonths(-3).format(  DateTimeFormatter.ISO_DATE_TIME )  );
        rangeQueryBuilder.lte( LocalDateTime.now().format(  DateTimeFormatter.ISO_DATE_TIME )  );

        BoolQueryBuilder boolQueryBuilder=QueryBuilders.boolQuery();
        boolQueryBuilder.must( rangeQueryBuilder );

        boolQueryBuilder.must( QueryBuilders.termQuery("business_id",businessId) );
        sourceBuilder.query(boolQueryBuilder);

        AggregationBuilder orderAgg = AggregationBuilders.terms("sku").field("sku_id")
                .subAggregation(AggregationBuilders.count("count").field("sku_id"))
                .order(BucketOrder.aggregation("count", false))
                .size(10);

        sourceBuilder.aggregation(orderAgg);
        searchRequest.source(sourceBuilder);

        try {
            SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
            Aggregations aggregations = searchResponse.getAggregations();
            if(aggregations==null ) return  Lists.newArrayList();

            var term = (Terms)aggregations.get("sku");
            var buckets = term.getBuckets();

            return buckets.stream().map(  b->   Long.valueOf( b.getKey().toString() ) ).collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }

    }


}

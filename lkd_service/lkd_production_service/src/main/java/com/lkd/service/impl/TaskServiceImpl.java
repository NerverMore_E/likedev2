package com.lkd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.lkd.common.VMSystem;
import com.lkd.config.TopicConfig;
import com.lkd.contract.SupplyCfg;
import com.lkd.contract.SupplyChannel;
import com.lkd.contract.TaskCompleteContract;
import com.lkd.dao.TaskDao;
import com.lkd.emq.MqttProducer;
import com.lkd.entity.TaskDetailsEntity;
import com.lkd.entity.TaskEntity;
import com.lkd.entity.TaskStatusTypeEntity;
import com.lkd.exception.LogicException;
import com.lkd.feignService.UserService;
import com.lkd.feignService.VMService;
import com.lkd.http.viewModel.CancelTaskViewModel;
import com.lkd.http.viewModel.TaskReportInfo;
import com.lkd.http.viewModel.TaskViewModel;
import com.lkd.service.TaskDetailsService;
import com.lkd.service.TaskService;
import com.lkd.service.TaskStatusTypeService;
import com.lkd.viewmodel.Pager;
import com.lkd.viewmodel.UserViewModel;
import com.lkd.viewmodel.UserWork;
import com.lkd.viewmodel.VendingMachineViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TaskServiceImpl extends ServiceImpl<TaskDao,TaskEntity> implements TaskService{
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private TaskDetailsService taskDetailsService;

    @Autowired
    private VMService vmService;

    @Autowired
    private TaskStatusTypeService statusTypeService;

    @Autowired
    private UserService userService;


    @Autowired
    private MqttProducer mqttProducer;


    @Override
    @Transactional(rollbackFor = {Exception.class},noRollbackFor = {LogicException.class})
    public boolean createTask(TaskViewModel taskViewModel) throws LogicException {
        checkCreateTask(taskViewModel.getInnerCode(),taskViewModel.getProductType());//验证
        if(hasTask(taskViewModel.getInnerCode(),taskViewModel.getProductType())) {
            throw new LogicException("该机器有未完成的同类型工单");
        }

        UserViewModel user = userService.getUser(taskViewModel.getUserId());
        //新增工单表记录
        TaskEntity taskEntity = new TaskEntity();
        BeanUtils.copyProperties(taskViewModel,taskEntity);//复制属性
        taskEntity.setTaskStatus(VMSystem.TASK_STATUS_CREATE);
        taskEntity.setProductTypeId(taskViewModel.getProductType());
        taskEntity.setTaskCode(this.generateTaskCode());//工单编号
        String userName = user.getUserName();
        taskEntity.setUserName(userName);
        taskEntity.setAddr(vmService.getVMInfo(taskViewModel.getInnerCode()).getNodeAddr());
        this.save(taskEntity);

        //如果是补货工单，向 工单明细表插入记录
        if(taskEntity.getProductTypeId() == VMSystem.TASK_TYPE_SUPPLY){
            taskViewModel.getDetails().forEach(d->{
                TaskDetailsEntity detailsEntity = new TaskDetailsEntity();
                BeanUtils.copyProperties( d,detailsEntity );
                detailsEntity.setTaskId(taskEntity.getTaskId());
                taskDetailsService.save(detailsEntity);
            });
        }
        updateTaskZSet(taskEntity,1);
        return true;


    }

    @Override
    public boolean accept(Long id) {
        TaskEntity task = this.getById(id);  //查询工单
        if(task.getTaskStatus()!= VMSystem.TASK_STATUS_CREATE ){
            throw new LogicException("工单状态不是待处理");
        }
        task.setTaskStatus( VMSystem.TASK_STATUS_PROGRESS );//修改工单状态为进行
        return this.updateById(task);
    }

    @Override
    public boolean cancelTask(long id, CancelTaskViewModel cancelVM) {
        TaskEntity task = this.getById(id);
        if(task.getTaskStatus() == VMSystem.TASK_STATUS_FINISH || task.getTaskStatus() == VMSystem.TASK_STATUS_CANCEL){
            throw new LogicException("该工单已经结束");
        }
        task.setTaskStatus(VMSystem.TASK_STATUS_CANCEL);
        task.setDesc(cancelVM.getDesc());

        updateTaskZSet(task,-1);
        return this.updateById(task);
    }




    @Override
    @Transactional
    public boolean completeTask(long id) {
        return  completeTask(id,0d,0d,"");
    }

    @Override
    public boolean completeTask(long id, Double lat, Double lon, String addr) {
        TaskEntity taskEntity = this.getById(id);
        if(taskEntity.getTaskStatus()== VMSystem.TASK_STATUS_FINISH  || taskEntity.getTaskStatus()== VMSystem.TASK_STATUS_CANCEL ){
            throw new LogicException("工单已经结束");
        }
        taskEntity.setTaskStatus(VMSystem.TASK_STATUS_FINISH);
        taskEntity.setAddr(addr);
        this.updateById(taskEntity);

        //如果是投放工单或撤机工单
        if(taskEntity.getProductTypeId()==VMSystem.TASK_TYPE_DEPLOY
                || taskEntity.getProductTypeId()==VMSystem.TASK_TYPE_REVOKE){
            noticeVMServiceStatus(taskEntity,lat,lon);
        }
        //如果是补货工单
        if(taskEntity.getProductTypeId()==VMSystem.TASK_TYPE_SUPPLY){
            noticeVMServiceSupply(taskEntity);
        }
        return true;
    }

    /**
     * 运维工单封装与下发
     * @param taskEntity
     */
    private void noticeVMServiceStatus(TaskEntity taskEntity,Double lat,Double lon){
        //向消息队列发送消息，通知售货机更改状态
        //封装协议
        TaskCompleteContract taskCompleteContract=new TaskCompleteContract();
        taskCompleteContract.setInnerCode(taskEntity.getInnerCode());//售货机编号
        taskCompleteContract.setTaskType( taskEntity.getProductTypeId() );//工单类型
        taskCompleteContract.setLat(lat);//纬度
        taskCompleteContract.setLon(lon);//经度
        //发送到emq
        try {
            mqttProducer.send( TopicConfig.COMPLETED_TASK_TOPIC,2, taskCompleteContract );
        } catch (Exception e) {
            log.error("发送工单完成协议出错");
            throw new LogicException("发送工单完成协议出错");
        }
    }


    @Override
    public List<TaskStatusTypeEntity> getAllStatus() {
        QueryWrapper<TaskStatusTypeEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .ge(TaskStatusTypeEntity::getStatusId,VMSystem.TASK_STATUS_CREATE);

        return statusTypeService.list(qw);
    }

    @Override
    public Pager<TaskEntity> search(Long pageIndex, Long pageSize, String innerCode, Integer userId, String taskCode,Integer status,Boolean isRepair,String start,String end) {
        Page<TaskEntity> page = new Page<>(pageIndex,pageSize);
        LambdaQueryWrapper<TaskEntity> qw = new LambdaQueryWrapper<>();
        if(!Strings.isNullOrEmpty(innerCode)){
            qw.eq(TaskEntity::getInnerCode,innerCode);
        }
        if(userId != null && userId > 0){
            qw.eq(TaskEntity::getUserId,userId);
        }
        if(!Strings.isNullOrEmpty(taskCode)){
            qw.like(TaskEntity::getTaskCode,taskCode);
        }
        if(status != null && status > 0){
            qw.eq(TaskEntity::getTaskStatus,status);
        }
        if(isRepair != null){
            if(isRepair){
                qw.ne(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
            }else {
                qw.eq(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
            }
        }
        if(!Strings.isNullOrEmpty(start) && !Strings.isNullOrEmpty(end)){
            qw
                    .ge(TaskEntity::getCreateTime,LocalDate.parse(start,DateTimeFormatter.ISO_LOCAL_DATE))
                    .le(TaskEntity::getCreateTime,LocalDate.parse(end,DateTimeFormatter.ISO_LOCAL_DATE));
        }
        //根据最后更新时间倒序排序
        qw.orderByDesc(TaskEntity::getUpdateTime);

        return Pager.build(this.page(page,qw));
    }


    /**
     * 补货协议封装与下发
     * @param taskEntity
     */
    private void noticeVMServiceSupply(TaskEntity taskEntity){

        QueryWrapper<TaskDetailsEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(TaskDetailsEntity::getTaskId,taskEntity.getTaskId());
        List<TaskDetailsEntity> details = taskDetailsService.list(qw);
        SupplyCfg supplyCfg = new SupplyCfg();
        supplyCfg.setInnerCode(taskEntity.getInnerCode());
        List<SupplyChannel> supplyChannels = Lists.newArrayList();
        details.forEach(d->{
            SupplyChannel channel = new SupplyChannel();
            channel.setChannelId(d.getChannelCode());
            channel.setCapacity(d.getExpectCapacity());
            supplyChannels.add(channel);
        });
        supplyCfg.setSupplyData(supplyChannels);

        //2.下发补货协议
        //发送到emq
        try {
            mqttProducer.send( TopicConfig.COMPLETED_TASK_TOPIC,2, supplyCfg );
        } catch (Exception e) {
            log.error("发送工单完成协议出错");
            throw new LogicException("发送工单完成协议出错");
        }

    }


    /**
     * 同一台设备下是否存在未完成的工单
     * @param innerCode
     * @param productionType
     * @return
     */
    private boolean hasTask(String innerCode,int productionType){
        QueryWrapper<TaskEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .select(TaskEntity::getTaskId)
                .eq(TaskEntity::getInnerCode,innerCode)
                .eq(TaskEntity::getProductTypeId,productionType)
                .le(TaskEntity::getTaskStatus,VMSystem.TASK_STATUS_PROGRESS);

        return this.count(qw) > 0;
    }

    private void checkCreateTask(String innerCode,int productType) throws LogicException {
        VendingMachineViewModel vmInfo = vmService.getVMInfo(innerCode);//根据设备编号查询设备
        if(vmInfo == null) throw new LogicException("设备校验失败");
        if(productType == VMSystem.TASK_TYPE_DEPLOY  && vmInfo.getVmStatus() == VMSystem.VM_STATUS_RUNNING){
            throw new LogicException("该设备已在运营");
        }

        if(productType == VMSystem.TASK_TYPE_SUPPLY  && vmInfo.getVmStatus() != VMSystem.VM_STATUS_RUNNING){
            throw new LogicException("该设备不在运营状态");
        }

        if(productType == VMSystem.TASK_TYPE_REVOKE  && vmInfo.getVmStatus() != VMSystem.VM_STATUS_RUNNING){
            throw new LogicException("该设备不在运营状态");
        }
    }

    /**
     * 生成工单编号
     * @return
     */
    private String generateTaskCode(){
        //日期+序号
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));  //日期字符串
        String key= "lkd.task.code."+date; //redis key
        Object obj = redisTemplate.opsForValue().get(key);
        if(obj==null){
            redisTemplate.opsForValue().set(key,1L, Duration.ofDays(1) );
            return date+"0001";
        }
        return date+  Strings.padStart( redisTemplate.opsForValue().increment(key,1).toString(),4,'0');
    }


    /**
     * 更新工单量列表
     * @param taskEntity
     * @param score
     */
    private void  updateTaskZSet(TaskEntity taskEntity,int score){
        String roleCode="1003";//运维员
        if(taskEntity.getProductTypeId().intValue()==2){//如果是补货工单
            roleCode="1002";//运营员
        }
        String key= VMSystem.REGION_TASK_KEY_PREF
                + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))
                +"."+ taskEntity.getRegionId()+"."+roleCode;
        redisTemplate.opsForZSet().incrementScore(key,taskEntity.getAssignorId(),score);
    }

    @Override
    public Integer getLeastUser(Long regionId, Boolean isRepair) {
        String roleCode="1002";
        if(isRepair){ //如果是维修工单
            roleCode="1003";
        }
        String key= VMSystem.REGION_TASK_KEY_PREF
                + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))
                +"."+ regionId+"."+roleCode;

        System.out.println("key:::::"+key);
        Set<Object> set = redisTemplate.opsForZSet().range(key, 0, 1);
        if(set==null || set.isEmpty()){
            throw  new LogicException("该区域暂时没有相关人员");
        }
        return (Integer) set.stream().collect( Collectors.toList()).get(0);
    }



    /**
     * 统计工单数量
     * @param start
     * @param end
     * @param repair 是否是运维工单
     * @param taskStatus
     * @return
     */
    private int taskCount( LocalDateTime start, LocalDateTime end ,Boolean repair ,Integer taskStatus ){
        LambdaQueryWrapper<TaskEntity> qw = new LambdaQueryWrapper<>();
        qw.ge(TaskEntity::getUpdateTime,start)
                .le(TaskEntity::getUpdateTime,end);
        //按工单状态查询
        if(taskStatus!=null){
            qw.eq(TaskEntity::getTaskStatus,taskStatus);
        }
        if(repair){//如果是运维工单
            qw.ne(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }else{
            qw.eq(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }
        return this.count(qw);
    }

    @Override
    public List<TaskReportInfo> getTaskReportInfo(LocalDateTime start, LocalDateTime end) {

        //运营工单总数total
        var supplyTotalFuture = CompletableFuture.supplyAsync(()->this.taskCount( start,end,false, null ));
        //运维工单总数
        var repairTotalFuture = CompletableFuture.supplyAsync(()->this.taskCount( start,end,true, null ));
        //完成的运营工单总数
        var completedSupplyFuture = CompletableFuture.supplyAsync(()->this.taskCount( start,end,false, VMSystem.TASK_STATUS_FINISH ));
        //完成的运维工单总数
        var completedRepairFuture = CompletableFuture.supplyAsync(()-> this.taskCount( start,end,true, VMSystem.TASK_STATUS_FINISH ));
        //拒绝掉的运营工单总数
        var cancelSupplyFuture = CompletableFuture.supplyAsync(()->this.taskCount( start,end,false, VMSystem.TASK_STATUS_CANCEL ));
        //拒绝掉的运维工单总数
        var cancelRepairFuture = CompletableFuture.supplyAsync(()->this.taskCount( start,end,true, VMSystem.TASK_STATUS_CANCEL ));
        // 获取运营人员数量
        var operatorCountFuture = CompletableFuture.supplyAsync(()-> userService.getOperatorCount());
        //获取运维人员总数
        var repairerCountFuture = CompletableFuture.supplyAsync(()-> userService.getRepairerCount());
        //并行处理
        CompletableFuture
                .allOf(supplyTotalFuture,
                        repairTotalFuture,
                        completedSupplyFuture,
                        completedRepairFuture,
                        cancelSupplyFuture,
                        cancelRepairFuture,
                        operatorCountFuture,
                        repairerCountFuture)
                .join();

        List<TaskReportInfo> result = Lists.newArrayList();
        var supplyTaskInfo = new TaskReportInfo();
        var repairTaskInfo = new TaskReportInfo();
        try {
            supplyTaskInfo.setTotal(supplyTotalFuture.get());
            supplyTaskInfo.setCancelTotal(cancelSupplyFuture.get());
            supplyTaskInfo.setCompletedTotal(completedSupplyFuture.get());
            supplyTaskInfo.setRepair(false);
            supplyTaskInfo.setWorkerCount(operatorCountFuture.get());
            result.add(supplyTaskInfo);

            repairTaskInfo.setTotal(repairTotalFuture.get());
            repairTaskInfo.setCancelTotal(cancelRepairFuture.get());
            repairTaskInfo.setCompletedTotal(completedRepairFuture.get());
            repairTaskInfo.setRepair(true);
            repairTaskInfo.setWorkerCount(repairerCountFuture.get());
            result.add(repairTaskInfo);
        }catch (Exception e){
            log.error("构建工单统计数据失败",e);
        }
        return result;
    }


    //根据工单状态，获取用户(当月)工单数
    private Integer getCountByUserId(Integer userId,Integer taskStatus,LocalDate start,LocalDateTime end){
        var qw = new LambdaQueryWrapper<TaskEntity>();
        qw
                .ge(TaskEntity::getUpdateTime,start)
                .le(TaskEntity::getUpdateTime,end);
        if(taskStatus != null ){
            qw.eq(TaskEntity::getTaskStatus,taskStatus);
        }
        if(userId != null) {
            qw.eq(TaskEntity::getUserId,userId);
        }
        return this.count(qw);
    }

    @Override
    public UserWork getUserWork(Integer userId, LocalDateTime start, LocalDateTime end) {
        var userWork = new UserWork();
        userWork.setUserId(userId);
        //并行处理提高程序吞吐量
        //获取用户完成工单数
        var workCountFuture = CompletableFuture
                .supplyAsync(()-> this.getCountByUserId(userId,VMSystem.TASK_STATUS_FINISH,start.toLocalDate(),end))
                .whenComplete((r,e)->{
                    if(e != null){
                        userWork.setWorkCount(0);
                        log.error("user work error",e);
                    }else {
                        userWork.setWorkCount(r);
                    }
                });
        //获取工单总数
        var totalFuture = CompletableFuture
                .supplyAsync(()->this.getCountByUserId(userId,null,start.toLocalDate(),end))
                .whenComplete((r,e)->{
                    if(e != null){
                        userWork.setWorkCount(0);
                        log.error("user work error",e);
                    }else {
                        userWork.setTotal(r);
                    }
                });
        //获取用户拒绝工单数
        var cancelCountFuture = CompletableFuture
                .supplyAsync(()-> this.getCountByUserId(userId,VMSystem.TASK_STATUS_CANCEL,start.toLocalDate(),end))
                .whenComplete((r,e)->{
                    if(e != null){
                        userWork.setCancelCount(0);
                        log.error("user work error",e);
                    }else {
                        userWork.setCancelCount(r);
                    }
                });
        //获取进行中得工单数
        var progressTotalFuture = CompletableFuture
                .supplyAsync(()->this.getCountByUserId(userId,VMSystem.TASK_STATUS_PROGRESS,start.toLocalDate(),end))
                .whenComplete((r,e)->{
                    if(e != null){
                        userWork.setProgressTotal(0);
                        log.error("user work error",e);
                    }else {
                        userWork.setProgressTotal(r);
                    }
                });
        CompletableFuture.allOf(workCountFuture,cancelCountFuture,progressTotalFuture).join();
        return userWork;
    }



    @Override
    public List<UserWork> getUserWorkTop10(LocalDate start, LocalDate end, Boolean isRepair,Long regionId) {
        var qw = new QueryWrapper<TaskEntity>();
        qw
                .select("count(user_id) as user_id,user_name")
                .lambda()
                .ge(TaskEntity::getUpdateTime,start)
                .le(TaskEntity::getUpdateTime,end)
                .eq(TaskEntity::getTaskStatus,VMSystem.TASK_STATUS_FINISH)
                .groupBy(TaskEntity::getUserName)
                .orderByDesc(TaskEntity::getUserId)
                .last("limit 10");
        if(regionId >0){
            qw.lambda().eq(TaskEntity::getRegionId,regionId);
        }
        if(isRepair){
            qw.lambda().ne(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }else {
            qw.lambda().eq(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }
        var result = this
                .list(qw)
                .stream()
                .map(t->{
                    var userWork = new UserWork();
                    userWork.setUserName(t.getUserName());
                    userWork.setWorkCount(t.getUserId());
                    return userWork;
                }).collect(Collectors.toList());
        return result;
    }





}

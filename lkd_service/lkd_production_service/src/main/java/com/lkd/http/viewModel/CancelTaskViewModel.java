package com.lkd.http.viewModel;

import lombok.Data;

@Data
public class CancelTaskViewModel {

    /**
     * 拒绝理由
     */
    private String desc;

}
